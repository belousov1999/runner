﻿using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Game
{
    public class EnemyManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private float _enemiesPerDistance;
        [SerializeField] private float _enemiesFarDistance;
        [SerializeField] private float _dontSpawnFinishDist;
        [SerializeField] private GameObject _enemyPrefab;
        [SerializeField] private List<float> _spawnXPoints;
        
        //Private
        private float _lastSpawnedDistance;
        private List<GameObject> _enemies = new List<GameObject>();

        //Private Static
        private static EnemyManager _instance;

        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            Player.Get().PlayerStats.OnFinishDistanceChanged += OnFinishDistanceUpdated;
        }

        public static EnemyManager Get()
            => _instance;

        public void Restart()
        {
            foreach (var enemy in _enemies)
                Destroy(enemy);
            _enemies.Clear();
        }

        void OnFinishDistanceUpdated(float distance)
        {
            var changeDistance = Mathf.Abs(distance - _lastSpawnedDistance);
            if (changeDistance > _enemiesPerDistance
                && distance > _dontSpawnFinishDist)
            {
                SpawnEnemies();
                _lastSpawnedDistance = distance;
            }
        }

        void SpawnEnemies()
        {
            var count = Random.Range(1, 3);
            _spawnXPoints.Shuffle();
            var playerPos = Player.Get().transform.position;
            for(var i = 0; i < count; i++)
            {
                var xPoint = _spawnXPoints[i];
                var enemy = Instantiate(_enemyPrefab);
                enemy.transform.position = new Vector3(xPoint, playerPos.y, playerPos.z + _enemiesFarDistance);
                _enemies.Add(enemy);
            }
        }
    }
}
