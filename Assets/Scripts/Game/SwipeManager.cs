﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class SwipeManager : MonoBehaviour
    {
        //Editor
        public float SWIPE_THRESHOLD = 20f;

        //Public
        /// <summary>
        /// First bool - True if Right swipe
        /// Second bool - True if Swipe ended
        /// </summary>
        public Action<bool, bool> OnSwipeEvent;

        //Private
        private Vector2 _fingerDown, _fingerUp;

        //Private Static
        private static SwipeManager _instance;

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(this.gameObject);
                return;
            }
            _instance = this;
        }

        public static SwipeManager Get()
            => _instance;

        void CheckSwipe()
        {
            if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
            {
                //Right swipe
                if (_fingerDown.x - _fingerUp.x < 0)
                    OnSwipeRight();

                //Left swipe
                else if (_fingerDown.x - _fingerUp.x > 0)
                    OnSwipeLeft();
            }
        }

        float verticalMove()
            => Mathf.Abs(_fingerDown.y - _fingerUp.y);

        float horizontalValMove()
            => Mathf.Abs(_fingerDown.x - _fingerUp.x);

        void OnSwipeLeft()
            => OnSwipeEvent?.Invoke(false, false);

        void OnSwipeRight()
            => OnSwipeEvent?.Invoke(true, false);

        void OnEndSwipe()
        => OnSwipeEvent?.Invoke(true, true);

        void Update()
        {
            if (EventSystem.current.currentSelectedGameObject != null) return;

            if (Input.GetMouseButtonDown(0))
            {
                _fingerDown = Input.mousePosition;
                _fingerUp = _fingerDown;
            }
            else if (Input.GetMouseButton(0))
            {
                _fingerUp = Input.mousePosition;
                CheckSwipe();
            }else if (Input.GetMouseButtonUp(0))
            {
                _fingerDown = Input.mousePosition;
                _fingerUp = _fingerDown;
                OnEndSwipe();
            }
        }

        private void OnDestroy()
        {
            if (_instance == this)
                _instance = null;
        }
    }
}
