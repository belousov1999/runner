﻿using Game;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HpText : GameUIElement
    {
        //Editor
        [SerializeField] private Text hpText;

        //Private Static
        private static string TEXT_PREFIX = "HP: ";

        private void Start()
        {
            OnHealthUpdated(Player.Get().PlayerStats.Health);
            Player.Get().PlayerStats.OnHealthChanged += OnHealthUpdated;
        }

        void OnHealthUpdated(int health)
        {
            hpText.text = TEXT_PREFIX + health;
        }
    }
}
