﻿using Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class DistanceText : GameUIElement
    {
        //Editor
        [SerializeField] private Text hpText;

        //Private Static
        private static string TEXT_PREFIX = "Distance: ";

        private void Start()
        {
            OnDistanceUpdated(Player.Get().PlayerStats.FinishDistance);
            Player.Get().PlayerStats.OnFinishDistanceChanged += OnDistanceUpdated;
        }

        void OnDistanceUpdated(float distance)
        {
            hpText.text = TEXT_PREFIX + (int)distance;
        }
    }
}
