﻿using System;
using UnityEngine;

namespace Game
{
    public class PlayerStats : MonoBehaviour
    {
        //Editor
        [SerializeField] private int _initialHealth;
        [SerializeField] private int _initialSpeed;
        [SerializeField] private float _decreasePerTime;

        //Public
        public int Health
        {
            get => _health; set
            {
                _health = value;
                OnHealthChanged?.Invoke(value);

                if (value <= 0)
                    GameStateManager.Get().CurrentState = Enums.EGameStates.STATE_LOSE;
            }
        }
        public float Speed { get; set; }

        public float FinishDistance
        {
            get => _finishDistance; set
            {
                _finishDistance = value;
                OnFinishDistanceChanged?.Invoke(value);
            }
        }

        public Action<int> OnHealthChanged;
        public Action<float> OnFinishDistanceChanged;

        //Private
        private int _health;
        private float _t, _finishDistance;

        private void Awake()
        {
            Speed = _initialSpeed;
        }

        private void Start()
        {
            Health = _initialHealth;
        }

        public void Restart()
        {
            Health = _initialHealth;
        }

        private void Update()
        {
            if (GameStateManager.Get().CurrentState != Enums.EGameStates.STATE_GAME)
            {
                _t = 0;
                return;
            }
            _t += Time.deltaTime;
            if (_t > _decreasePerTime)
            {
                _t -= _decreasePerTime;
                Health--;
            }
        }
    }
}
