﻿using Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class RestartButton : MonoBehaviour
    {
        //Editor
        [SerializeField] private Button restButton;

        private void Start()
        {
            restButton.onClick.AddListener(OnRestart);
        }

        void OnRestart()
        {
            Player.Get().Restart();
            EnemyManager.Get().Restart();
            GameStateManager.Get().CurrentState = Enums.EGameStates.STATE_GAME;
        }
    }
}
