﻿using Enums;
using Game;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class EndGameText : GameUIElement
    {
        //Editor
        [SerializeField] private Text _endGameText;

        private void Start()
        {
            OnGameStateUpdated(GameStateManager.Get().CurrentState);
            GameStateManager.Get().OnGameStateChangedEvent += OnGameStateUpdated;
        }

        void OnGameStateUpdated(EGameStates newState)
        {
            Debug.Log("EndGameState: "+newState);
            if (newState == EGameStates.STATE_WIN)
                _endGameText.text = "WIN";
            else if (newState == EGameStates.STATE_LOSE)
                _endGameText.text = "Lose";
        }
    }
}
