﻿using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI;
using UnityEngine;

namespace Game
{
    public class GameStateManager : MonoBehaviour
    {

        //Public
        public EGameStates CurrentState
        {
            get => _currentState; set
            {
                _currentState = value;
                OnGameStateChangedEvent?.Invoke(value);
            }
        }
        public Action<EGameStates> OnGameStateChangedEvent;

        //Private
        private EGameStates _currentState;

        //Private Static
        private static GameStateManager _instance;


        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            CurrentState = EGameStates.STATE_GAME;
        }

        public static GameStateManager Get()
            => _instance;

    }
}
