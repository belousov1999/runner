﻿using Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SpeedSlider : GameUIElement
    {
        //Editor
        [SerializeField] private Text _valueText;
        [SerializeField] private Slider _slider;

        //Private Static
        private static string TEXT_PREFIX = "Speed: ";

        private void Start()
        {
            OnSpeedUpdated(Player.Get().PlayerStats.Speed);
            _slider.onValueChanged.AddListener(OnSpeedUpdated);
        }

        void OnSpeedUpdated(float speed)
        {
            _valueText.text = TEXT_PREFIX + speed.ToString("F2");
            Player.Get().PlayerStats.Speed = speed;
        }
    }
}
