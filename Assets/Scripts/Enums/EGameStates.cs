﻿
namespace Enums
{
    public enum EGameStates
    {
        STATE_GAME,
        STATE_LOSE,
        STATE_WIN
    }
}
