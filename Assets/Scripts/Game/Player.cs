﻿using System;
using UnityEngine;

namespace Game
{
    public class Player : MonoBehaviour
    {
        //Editor
        [SerializeField] private float _timeWithoutDamage;
        [SerializeField] private Transform _finishTrans;
        [SerializeField] private PlayerStats _playerStats;

        //Public
        public PlayerStats PlayerStats => _playerStats;

        //Private
        private Vector3 _startPosition;
        private bool _movingSide, _movingRight;
        private float _invurTime;

        //Private Static
        private static Player _instance;

        private void Awake()
        {
            _instance = this;
            _startPosition = transform.position;
        }

        private void Start()
        {
            SwipeManager.Get().OnSwipeEvent += OnSwipe;
        }

        public static Player Get()
            => _instance;

        public void Restart()
        {
            transform.position = _startPosition;
            PlayerStats.Restart();
        }

        void OnSwipe(bool isRight, bool isSwipeEnded)
        {
            _movingSide = !isSwipeEnded;
            _movingRight = isRight;
        }

        void CalculateFinishDistance()
        {
            PlayerStats.FinishDistance = Mathf.Abs(_finishTrans.position.z - transform.position.z);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.SetActive(false);
                if (_invurTime <= 0)
                {
                    PlayerStats.Health--;
                    _invurTime = _timeWithoutDamage;
                }
            }
            else if (collision.gameObject.CompareTag("Wall") && _invurTime <= 0)
            {
                PlayerStats.Health--;
                _invurTime = _timeWithoutDamage;
            }

            if (collision.gameObject.CompareTag("Finish"))
                GameStateManager.Get().CurrentState = Enums.EGameStates.STATE_WIN;
        }

        private void Update()
        {
            if (GameStateManager.Get().CurrentState != Enums.EGameStates.STATE_GAME)
                return;

            if (_invurTime > 0)
                _invurTime -= Time.deltaTime;

            var pos = transform.position;
            pos.z += PlayerStats.Speed * Time.deltaTime;

            if (_movingSide)
            {
                var xSpeed = (_movingRight ? 1 : -1) * PlayerStats.Speed;
                pos.x += xSpeed * Time.deltaTime;
            }
            transform.position = pos;
            CalculateFinishDistance();
        }
    }
}
