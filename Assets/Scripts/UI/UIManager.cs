﻿using Enums;
using Game;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private GameUIElement[] _uiElements;
        
        private void Start()
        {
            var gameStateManager = GameStateManager.Get();
            gameStateManager.OnGameStateChangedEvent += OnGameStateChanged;
        }

        void OnGameStateChanged(EGameStates newState)
        {
            foreach (var ui in _uiElements)
                ui.gameObject.SetActive(ui.ShowStates.Contains(newState));
        }

    }
}
