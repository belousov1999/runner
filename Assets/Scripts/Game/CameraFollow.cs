﻿using UnityEngine;

namespace Game
{
    public class CameraFollow : MonoBehaviour
    {
        //Editor
        [SerializeField] private Transform _cameraTarget;

        //Private
        private float _targetZPosition;

        private void Start()
        {
            _targetZPosition = _cameraTarget.position.z;
        }

        private void LateUpdate()
        {
            var zPos = _cameraTarget.position.z;
            var camPos = transform.position;
            camPos.z += zPos - _targetZPosition;
            transform.position = camPos;
            _targetZPosition = zPos;
        }
    }
}
